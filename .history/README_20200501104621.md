# BravoChromeExtension

Built using vue-chrome-extension-boilerplate (vue-chrome-extension-boilerplate
)

## Scripts

```json
// build extension and watch for changes
npm run dev

// build extension zip
npm run build

// lint all source files
npm run lint
```
