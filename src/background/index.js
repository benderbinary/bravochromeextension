const stopword = require('stopword')
const axios = require('axios')

// OnInstall handler
chrome.runtime.onInstalled.addListener(details => {
  console.log(details)
})

function getRss () {
  return axios
    .get('http://localhost:3001/rss')
    .then(response => {
      console.log(response.data, 'in getRss')
    })
    .catch(error => {
      console.log(error)
      this.isError = true
    })
    .finally(() => {
      this.loading = false
    })
}

//
function removeStopwords (result) {
  const initialTextValue = result
  const filteredTextValue = stopword.removeStopwords(
    initialTextValue[0].split(' ')
  )
  // remove empty and one-word items
  const filteredWordsArray = filteredTextValue.filter(el => {
    return el !== null && el.length > 1
  })
  // create Map
  const wordsMap = filteredWordsArray.reduce(
    (acc, e) => acc.set(e, (acc.get(e) || 0) + 1),
    new Map()
  )

  let wordsMapAsc = Array.from(
    new Map(
      [...wordsMap].sort(
        (a, b) => (a[1] > b[1] && 1) || (a[1] === b[1] ? 0 : -1)
      )
    ).keys()
  )

  wordsMapAsc = wordsMapAsc.slice(-10)

  console.log(wordsMapAsc)

  const responseVal = getRss()

  console.log(responseVal)

  chrome.runtime.onConnect.addListener(function (port) {
    port.postMessage({ backResponse: responseVal })
  })
}

// opens a communication port
chrome.runtime.onConnect.addListener(function (port) {
  // listen for every message passing throw it
  port.onMessage.addListener(function (o) {
    // if the message comes from the popup
    if (o.from && o.from === 'popup' && o.start && o.start === 'onPopup') {
      chrome.tabs.executeScript(
        null,
        {
          code: 'document.body.innerText',
        },
        removeStopwords
      )
    }
  })
})
